template <class T>
Conjunto<T>::Conjunto():_raiz(nullptr),_size(0){
}

template <class T>
Conjunto<T>::~Conjunto() {
    borrarNodos(_raiz);
}

template <class T>
void Conjunto<T>::borrarNodos(Conjunto::Nodo *r) {
    if (r == nullptr){
        return;
    } else if (r->izq != nullptr && r->der != nullptr){ //Tiene dos hijos
        borrarNodos(r->izq);
        borrarNodos(r->der);
        _size = _size - 1;
        delete r;
    } else if (r->izq != nullptr && r->der == nullptr){ //Tiene solo hijo izquierdo
        borrarNodos(r->izq);
        _size = _size - 1;
        delete r;
    } else if (r->izq == nullptr && r->der != nullptr){ //Tiene solo hijo derecho
        borrarNodos(r->der);
        _size = _size - 1;
        delete r;
    } else { //Es hoja.
        delete(r);
        _size = _size - 1;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* buscador = buscar(clave);

    if(buscador){
        return buscador->valor == clave;
    }
}


template <class T>
void Conjunto<T>::insertar(const T& clave) {
    //Si pertenece entonces no lo insertamos
    if (pertenece(clave)){
        return;
    }
    //Si no pertenece y el arbol es vacio insertamos el nodo
    if (_raiz == nullptr){
        _raiz = new Nodo(clave);
        _size = 1;
    }
    else {
        //Recorremos el arbol y guardamos el ultimo nodo al que hay que insertar el nodo con la clave
        Nodo *i = _raiz;
        Nodo *p = nullptr;
        while (i != nullptr) {
            if (clave < i->valor) {
                p = i;
                i = i->izq;
            } else {
                p = i;
                i = i->der;
            }
        }
        //Ahora p guarda el valor del ultimo nodo no null al que hay que insertarle "clave"
        Nodo *nodoAInsertar = new Nodo(clave);
        nodoAInsertar->padre = p;
        if (clave < p->valor) {
            p->izq = nodoAInsertar;
        } else {
            p->der = nodoAInsertar;
        }
        _size++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (!pertenece(clave)){
        return;
    }
    // Buscamos al nodo ya sabiendo que el nodo que queremos borrar pertenece.
    Nodo* nodoClave = buscar(clave);

    //Si el nodo es hoja lo borramos.
    if (nodoClave->izq == nullptr && nodoClave->der == nullptr){
        if (_raiz->valor == clave){
            delete(_raiz);
            _raiz = nullptr;
            _size = 0;
        } else {
            //Si la clave a remover no es la raiz y es una hoja ...
            Nodo* padreDelNodo = nodoClave->padre;
            //Me fijo si es hoja izquierda o derecha y actuo acorde...
            if (clave < padreDelNodo->valor){
                padreDelNodo->izq = nullptr;
            } else {
                padreDelNodo->der = nullptr;
            }
            delete (nodoClave);
            _size--;
        }
        return;
    }
    //El nodo que tenemos que borrar tiene un solo hijo
    //Si tiene hijo derecho...
    if(nodoClave->izq == nullptr && nodoClave->der != nullptr){
        if (_raiz->valor == clave){
            Nodo* hijoDerecho = _raiz->der;
            hijoDerecho->padre = nullptr;
            delete _raiz;
            _raiz = hijoDerecho;
            _size--;
        } else {
            //Me hago un puntero al padre del nodo y al hijo derecho del nodo
            Nodo *padreDelNodo = nodoClave->padre;
            Nodo *hijoDerechoDelNodo = nodoClave->der;

            hijoDerechoDelNodo->padre = padreDelNodo;

            //pregunto si el nodoClave es hijo derecho o izquierdo para saber en donde ubicar a su hijo
            if(padreDelNodo->izq != nullptr) {
                if (nodoClave->valor == padreDelNodo->izq->valor) {
                    padreDelNodo->izq = hijoDerechoDelNodo;
                }
            }
            if(padreDelNodo->der != nullptr) {
                if (nodoClave->valor == padreDelNodo->der->valor) {
                    padreDelNodo->der = hijoDerechoDelNodo;
                }
            }
            delete (nodoClave);
            nodoClave = nullptr;
            _size--;
        }
       return;
    }
    //Si tiene hijo izquierdo...
    if(nodoClave->izq != nullptr && nodoClave->der == nullptr){
        if (_raiz->valor == clave){
            Nodo* hijoIzquierdo = _raiz->izq;
            hijoIzquierdo->padre = nullptr;
            delete _raiz;           // Elimina la raíz actual
            _raiz = hijoIzquierdo; // Asigna el hijo izquierdo como nueva raíz
            _size--;
        } else {
            //Me hago un puntero al padre del nodo y al hijo izquierdo del nodo
            Nodo *padreDelNodo = nodoClave->padre;
            Nodo *hijoIzquierdoDelNodo = nodoClave->izq;

            hijoIzquierdoDelNodo->padre = padreDelNodo;

            //pregunto si el nodoClave es hijo derecho o izquierdo para saber en donde ubicar a su hijo
            if(padreDelNodo->izq != nullptr) {
                if (nodoClave->valor == padreDelNodo->izq->valor) {
                    padreDelNodo->izq = hijoIzquierdoDelNodo;
                }
            }
            if(padreDelNodo->der != nullptr) {
                if (nodoClave->valor == padreDelNodo->der->valor) {
                    padreDelNodo->der = hijoIzquierdoDelNodo;
                }
            }
            delete (nodoClave);
            nodoClave = nullptr;
            _size--;
        }
        return;
    }

    //El nodo que tenemos que borrar tiene dos hijos
    if(nodoClave->izq && nodoClave->der){
        int sig = siguiente(clave);
        remover(sig);
        nodoClave->valor = sig;
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    // Buscamos al nodo
    Nodo* i = buscar(clave);

    // Ya sabemos que el nodo i tiene a "clave" en su valor
    if(i->der != nullptr){
        //Busco el minimo del sub-arbol derecho
        Nodo* i_d = i->der;
        while (i_d -> izq != nullptr){
            i_d = i_d->izq;
        }
        return i_d -> valor;
    } else {
        //subo hasta encontrar el primer mayor
        Nodo* padre = i->padre;
        while(padre->valor < i->valor){
            padre = padre->padre;
        }
        return padre->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* i = _raiz;
    while (i ->izq != nullptr){
        i = i->izq;
    }
    return i->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* i = _raiz;
    while (i->der != nullptr){
        i = i->der;
    }
    return i->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _size;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}