#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();
    bool operator==(Fecha o);
    void incrementar_dia();

  private:
    int _mes;
    int _dia;
};
Fecha::Fecha(int mes, int dia):_mes(mes),_dia(dia) {}

int Fecha::mes() {
    return _mes;
}

int Fecha::dia() {
    return _dia;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}

void Fecha::incrementar_dia() {
    if (dias_en_mes(_mes) == _dia){
        _mes = _mes + 1;
        _dia = 1;
    } else {
        _dia = _dia + 1;
    }
}


// Ejercicio 11, 12

// Clase Horario

class Horario {
  public:
    // Completar declaraciones funciones
    Horario(uint hora, uint min);
    int hora();
    int min();
    void incrementar_dia();
    bool operator==(Horario o);
    bool operator<(Horario o);

  private:
    int _hora;
    int _min;
};

Horario::Horario(uint hora, uint min):_hora(hora),_min(min){}
int Horario::hora() {
    return _hora;
}

int Horario::min() {
    return _min;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario o) {
    bool mismaHora = o.hora() == this->hora();
    bool mismosMinutos = o.min() == this->min();
    return mismaHora && mismosMinutos;
}

bool Horario::operator<(Horario o) {
    if (this->hora() == o.hora()){
        return this->min() < o.min();
    }
    else if (this->hora() < o.hora()) {
        return true;
    }
    else {
        return false;
    }
}

// Ejercicio 13s

// Clase Recordatorio

class Recordatorio : public error_code {
  public:
    // Completar declaraciones funciones
    Recordatorio(Fecha f, Horario horario, string mensaje);
    Fecha fecha();
    Horario horario();
    string mensaje();
    bool operator==(Recordatorio o);
    bool operator<(Recordatorio o);

  private:
    Fecha _fecha;
    Horario _horario;
    string _mensaje;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, std::string mensaje):_fecha(fecha),_horario(horario),_mensaje(mensaje) {}

Fecha Recordatorio::fecha() {
    return _fecha;
}

Horario Recordatorio::horario(){
    return _horario;
}

string Recordatorio::mensaje() {
    return _mensaje;
}

bool Recordatorio::operator<(Recordatorio o) {
    return (this->horario() < o.horario());
}

bool Recordatorio::operator==(Recordatorio o) {
    return (this->horario() == o.horario());
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.fecha().dia() << "/"  << r.fecha().mes() << " " << r.horario().hora() << ":" << r.horario().min();
    return os;
}

// Ejercicio 14

// Clase Agenda

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        list<Recordatorio> _recordatorios;
        Fecha _hoy;
};

Agenda::Agenda(Fecha fecha_inicial):_hoy(fecha_inicial), _recordatorios(){}

void Agenda::agregar_recordatorio(Recordatorio rec){
    _recordatorios.push_back(rec);
    _recordatorios.sort();
}

Fecha Agenda::hoy() {
    return _hoy;
}

void Agenda::incrementar_dia() {
    _hoy.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> res;
    for (Recordatorio r: _recordatorios) {
        if (r.fecha() == _hoy){
            res.push_back(r);
        }
    }
    return res;
}


ostream& operator<<(ostream& os, Agenda a){
    a.recordatorios_de_hoy().sort();
    os << a.hoy() << endl;
    os << "=====" << endl;
    for (Recordatorio r:a.recordatorios_de_hoy()) {
        os << r << endl;
    }
    return os;
}