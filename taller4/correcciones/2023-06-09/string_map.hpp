template <typename T>
string_map<T>::string_map():raiz(nullptr),_size(0){
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {
    // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.
    *this = aCopiar;
}

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    /*
     Queremos hacer a = b, para hacer esto borramos lo que teniamos antes en a y le ponemos lo que tenemos en b
     como la funcion devuelve un & osea una referencia tenemos que desreferenciar el resultado.

     Si el diccionario que nos dan es vacio... retornamos uno vacio.
     */
    _destroyNodes(raiz);
    if (d.raiz == nullptr){
        _size = 0;
        raiz = nullptr;
        return *this;
    }

    // Sino... copiamos a partir de la raiz del nuevo diccionario al diccionario viejo.
    raiz = copiar(d.raiz);
    return *this;
}

template <typename T>
string_map<T>::~string_map(){
    _destroyNodes(raiz);
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // Opcional => no se hace ;)
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if (raiz == nullptr){
        return 0;
    }
    Nodo* node = raiz;
    // Agregar el caso en el que la clave no este
    for (char i:clave) {
        if (node->siguientes[int(i)] == nullptr){
            return 0;
        } else {
            node = node->siguientes[int(i)];
        }
    }
    if (node->definicion != nullptr){
        return 1;
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    //Precondicion la clave esta definida
    //Recorro el trie comenzando por la raiz
    Nodo* r = raiz;
    //Por cada una de las letras en clave... actualizo el nodo
    for (char i:clave) {
        int codigoASCII = int(i);
        r = r->siguientes[codigoASCII];
    }
    //Ahora el nodo r es el que contiene la definicion de la clave

    return *(r->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    //Precondicion la clave esta definida
    //Recorro el trie comenzando por la raiz
    Nodo* r = raiz;
    //Por cada una de las letras en clave... actualizo el nodo
    for (char i:clave) {
        int codigoASCII = int(i);
        r = r->siguientes[codigoASCII];
    }
    //Ahora el nodo r es el que contiene la definicion de la clave

    return *(r->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    if (count(clave) == 0) {
        return;
    }
    Nodo* ultimoNodo = nullptr;
    Nodo* it = raiz;

    int ultimaPosicion = 0;
    int posicion = 0;

    for (char i : clave) {
        if ((cantHijos(it) > 1 || (it->definicion != nullptr)) && posicion != clave.size()-1){
            ultimoNodo = it;
            ultimaPosicion = posicion;
        }
        it = it->siguientes[int(i)];
        posicion++;
    }

    // it es el último nodo (el que contiene el significado de clave).
    // ultimaPosicion es la última letra dentro de clave que NO tengo que borrar.
    // ultimoNodo es el último nodo que NO tengo que borrar

    //Si ultimoNodo != it => tengo nodos en el medio inecesarios.
    //Los borro hasta llegar a it y bajo en 1 el size

    _size --;
    delete it->definicion;
    it->definicion = nullptr;
    if (cantHijos(it) > 0){
        return;
    }

    if (ultimoNodo == nullptr){
        _destroyNodes(raiz);
        raiz = nullptr;
    } else {
        _destroyNodes(ultimoNodo->siguientes[int(clave[ultimaPosicion+1])]);
        ultimoNodo->siguientes[int(clave[ultimaPosicion+1])] = nullptr;
    }

}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;
}

template <typename T>
void string_map<T>::_destroyNodes(string_map::Nodo *n) {
    //Si tengo una hoja la borro y bajo la longitud (cantidad de nodos)
    if(n == NULL){
        return;
    }
    for(int i=0; i < n->siguientes.size(); i++ ){
        if(n->siguientes[i] != NULL){
            _destroyNodes(n->siguientes[i]);

        }
    }
    delete n->definicion;
    delete n;
}
template <typename T>
void string_map<T>::insert(const pair<std::string, T> &keyValue) {
    int i = 0;
    string palabra = keyValue.first;
    //Si estoy insertando en un diccionario vacio inicializo la raiz
    if (raiz == nullptr){
        Nodo* n = new Nodo();
        raiz = n;
    }
    Nodo* it = raiz;
    //Recorro el trie hasta llegar a la ultima letra de la palabra
    //Asignando en el camino a cada uno de los nodos la letra de la palabra.
    while (i < palabra.size()){
        char letra = keyValue.first[i];
        int codigoAscii = int(letra);
        vector<Nodo*>& siguientes = it->siguientes;
        if(siguientes[codigoAscii] != nullptr){
            it = it->siguientes[codigoAscii];
            i++;
        } else {
            Nodo* nuevoNodo = new Nodo();
            it->siguientes[codigoAscii] = nuevoNodo;
            it = it->siguientes[codigoAscii];
            i++;
        }
    }
    //Ahora me fijo si en it tengo como valor la palabra dada por parametro
    //Si la clave ya se encuentra en el trie la redefinimos.
    if (it->definicion != nullptr){
        delete it->definicion;
        it->definicion = new T(keyValue.second);
        return;
    } else {
        it->definicion = new T(keyValue.second);
        _size++;
        return;
    }
}