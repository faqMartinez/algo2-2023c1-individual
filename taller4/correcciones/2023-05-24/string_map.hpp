template <typename T>
string_map<T>::string_map():raiz(nullptr),_size(0){
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    // COMPLETAR
}

template <typename T>
string_map<T>::~string_map(){
    _destroyNodes(raiz);
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if (raiz == nullptr){
        return 0;
    }
    Nodo* node = raiz;
    int i = 0;
    char caracter = clave[i];
    int posicionEnAscii = int(caracter);
    while (i < clave.size()){
        if (node->siguientes[posicionEnAscii] == nullptr){
            return 0;
        } else {
            node = node->siguientes[posicionEnAscii];
            i++;
        }
    }
    if (node->definicion != nullptr){
        return 1;
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    // COMPLETAR
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    // COMPLETAR
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    // COMPLETAR
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    // bool longitudNula = (_size == 0);
    // bool sinDefiniciones = true;
    // Nodo* n = raiz;
    // //Caso base soy una hoja osea todos mis 256 siguientes son nullptr
    // if (n == nullptr){
    //     return longitudNula;
    // } else {
    //     for (Nodo* i: n->siguientes) {

    //     }
    // }
    // //
    // while (n != nullptr){
    //     for (Nodo* i: n->siguientes) {
    //         if (i != nullptr){

    //         }
    //     }
    // }

    // return  ;
    return _size == 0;
}

template <typename T>
void string_map<T>::_destroyNodes(string_map::Nodo *n) {
    //Si tengo una hoja la borro y bajo la longitud (cantidad de nodos)

    if (n == nullptr){
        _size = 0;
        return;
    }

    if (esHoja(n)){
        delete n;
        _size --;
        return;
    }
    //Si no es hoja entonces tiene nodos en "siguiente"
    //En ese caso borra cada uno de esos nodos y borra al nodo.
    for (Nodo* node:n->siguientes) {
            _destroyNodes(node);
            delete n;
            _size --;
    }
}
template <typename T>
void string_map<T>::insert(const pair<std::string, T> &keyValue) {
    if (count(keyValue.first) > 0){
        return;
    }
    int i = 0;
    char letra = keyValue.first[i];
    string palabra = keyValue.first;
    Nodo* it = raiz;

    //Recorro el trie hasta llegar a la ultima letra de la palabra
    while (i < palabra.size()){
        int codigoAscii = int(letra);
        if(it->siguientes[codigoAscii] != nullptr){
            it = it->siguientes[codigoAscii];
            i++;
        } else {
            Nodo* nuevoNodo = new Nodo();
            it->siguientes[codigoAscii] = nuevoNodo;
            it = it->siguientes[codigoAscii];
            i++;
        }
    }

    //Ahora me fijo si en it tengo como valor la palabra dada por parametro

    return;
}
