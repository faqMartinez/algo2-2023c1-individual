template <typename T>
string_map<T>::string_map():raiz(nullptr),_size(0){
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {
    // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.
    *this = aCopiar;
}

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    /*
     Queremos hacer a = b, para hacer esto borramos lo que teniamos antes en a y le ponemos lo que tenemos en b
     como la funcion devuelve un & osea una referencia tenemos que desreferenciar el resultado.

     Si el diccionario que nos dan es vacio... retornamos uno vacio.
     */
    _destroyNodes(raiz);
    if (d.raiz == nullptr){
        _size = 0;
        raiz = nullptr;
        return *this;
    }

    // Sino... copiamos a partir de la raiz del nuevo diccionario al diccionario viejo.
    raiz = copiar(d.raiz);
    return *this;
}

template <typename T>
string_map<T>::~string_map(){
    _destroyNodes(raiz);
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // Opcional => no se hace ;)
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if (raiz == nullptr){
        return 0;
    }
    Nodo* node = raiz;
    // Agregar el caso en el que la clave no este
    for (char i:clave) {
        if (node->siguientes[int(i)] == nullptr){
            return 0;
        } else {
            node = node->siguientes[int(i)];
        }
    }
    if (node->definicion != nullptr){
        return 1;
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    //Precondicion la clave esta definida
    //Recorro el trie comenzando por la raiz
    Nodo* r = raiz;
    //Por cada una de las letras en clave... actualizo el nodo
    for (char i:clave) {
        int codigoASCII = int(i);
        r = r->siguientes[codigoASCII];
    }
    //Ahora el nodo r es el que contiene la definicion de la clave

    return *(r->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    //Precondicion la clave esta definida
    //Recorro el trie comenzando por la raiz
    Nodo* r = raiz;
    //Por cada una de las letras en clave... actualizo el nodo
    for (char i:clave) {
        int codigoASCII = int(i);
        r = r->siguientes[codigoASCII];
    }
    //Ahora el nodo r es el que contiene la definicion de la clave

    return *(r->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    // COMPLETAR
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
     bool longitudNula = (_size == 0);
     bool sinDefiniciones = true;
     Nodo* n = raiz;
     //Caso base soy una hoja osea todos mis 256 siguientes son nullptr
     if (n == nullptr){
         return longitudNula;
     } else {
         for (Nodo* i: n->siguientes) {

         }
     }

     while (n != nullptr){
         for (Nodo* i: n->siguientes) {
             if (i != nullptr){

             }
         }
     }
    return _size == 0;
}

template <typename T>
void string_map<T>::_destroyNodes(string_map::Nodo *n) {
    //Si tengo una hoja la borro y bajo la longitud (cantidad de nodos)
    if (n == nullptr){
        return;
    }

    if (esHoja(n)){
        delete n;
        return;
    }
    //Si no es hoja entonces tiene nodos en "siguiente"
    //En ese caso borra cada uno de esos nodos y borra al nodo.
    for (Nodo* node:n->siguientes) {
        if (node != nullptr){
            _destroyNodes(node);
        }
    }
    delete n;
    _size = 0;
    return;
}
template <typename T>
void string_map<T>::insert(const pair<std::string, T> &keyValue) {
    int i = 0;
    string palabra = keyValue.first;
    //Si estoy insertando en un diccionario vacio inicializo la raiz
    if (raiz == nullptr){
        Nodo* n = new Nodo();
        raiz = n;
    }
    Nodo* it = raiz;
    //Recorro el trie hasta llegar a la ultima letra de la palabra
    //Asignando en el camino a cada uno de los nodos la letra de la palabra.
    while (i < palabra.size()){
        char letra = keyValue.first[i];
        int codigoAscii = int(letra);
        vector<Nodo*>& siguientes = it->siguientes;
        if(siguientes[codigoAscii] != nullptr){
            it = it->siguientes[codigoAscii];
            i++;
        } else {
            Nodo* nuevoNodo = new Nodo();
            it->siguientes[codigoAscii] = nuevoNodo;
            it = it->siguientes[codigoAscii];
            i++;
        }
    }
    //Ahora me fijo si en it tengo como valor la palabra dada por parametro
    //Si la clave ya se encuentra en el trie la redefinimos.
    if (it->definicion != nullptr){
        delete it->definicion;
        it->definicion = new T(keyValue.second);
        return;
    } else {
        it->definicion = new T(keyValue.second);
        _size++;
        return;
    }
}