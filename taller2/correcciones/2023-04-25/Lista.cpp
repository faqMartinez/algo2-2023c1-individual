#include "Lista.h"
#include <cassert>

Lista::Lista() {
    _lst = nullptr;
    _fst = nullptr;
    _size = 0;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    _destroyNodes(_fst);
}

void Lista::_destroyNodes(Lista::Nodo* init) {
    Nodo* i = init;
    if (i == nullptr){
        _fst = nullptr;
        _lst = nullptr;
        _size = 0;
        return;
    } else {
        while (i != nullptr){
            Nodo* nxt = i->nxt;
            delete i;
            i = nxt;
        }
        _fst = nullptr;
        _lst = nullptr;
        _size = 0;
        return;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    _destroyNodes(_fst);
    Nodo* i = aCopiar._fst;
    while(i != nullptr){
        // (*i).value === i->value
        agregarAtras(i->value);
        i = i->nxt;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo *newNode = new Nodo(
            elem,
            _fst,
            nullptr
    );
    if (_fst == nullptr){
        _fst = newNode;
        _lst = newNode;
    } else {
        _fst->prv = newNode;
        _fst = newNode;
    }
    _size++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* newNode = new Nodo(
            elem,
            nullptr,
            _lst
            );
    if (_size == 0){
        _fst = newNode;
        _lst = newNode;
    } else {
        _lst->nxt = newNode;
        _lst = newNode;
    }
    _size++;
}

void Lista::eliminar(Nat i) {
    if (_size == 1){
        delete _fst;
        _fst = nullptr;
        _lst = nullptr;
    } else if (i == 0) {
        Nodo* n = _fst;
        _fst = _fst->nxt;
        _fst->prv = nullptr;
        delete n;
    } else if (i == _size - 1) {
        Nodo* n = _lst;
        _lst = _lst->prv;
        _lst->nxt = nullptr;
        delete n;
    } else {
        searchAndDestroy(i);
    }
    _size--;
}

void Lista::searchAndDestroy(int position) {
    Nodo* i = _fst;
    int counter = 0;
    while(i != nullptr){
        if(position == counter){
            Nodo* c = i->nxt;
            Nodo* a = i->prv;
            a->nxt = c;
            c->prv = a;
            delete i;
            return;
        } else {
            counter++;
            i = i->nxt;
        }
    }
}
int Lista::longitud() const {
    return _size;
}

const int& Lista::iesimo(Nat i) const {
    int j = 0;
    Nodo* n = _fst;
    while (j != i){
        n = n->nxt;
        j++;
    }
    return n->value;
}

int& Lista::iesimo(Nat i) {
    int j = 0;
    Nodo* n = _fst;
    while (j != i){
        n = n->nxt;
        j++;
    }
    return n->value;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
